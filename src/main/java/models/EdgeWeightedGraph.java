package models;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.PriorityQueue;

public class EdgeWeightedGraph {
    private int V;
    private int E;
    private LatLng[] coo;
    private Bag<Edge>[] adj;
    private Bag<Edge>[] inc;

    private static final int rNum = 40;
    double[] verArr = new double[rNum + 1];
    double[] horArr = new double[rNum + 1];
    private List<List<Integer>> tiles;

    public EdgeWeightedGraph(boolean t) {
        this.loadGraph();
    }

    public EdgeWeightedGraph() {
        this.loadGraph();
        this.loadCoordinates();
    }

    public EdgeWeightedGraph(File file) {
        String line = null;

        try {
            FileReader fileReader = new FileReader(file);
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            this.V = Integer.parseInt(bufferedReader.readLine().trim());
            this.E = Integer.parseInt(bufferedReader.readLine().trim());

            adj = (Bag<Edge>[]) new Bag[V + 1];
            for (int v = 1; v <= V; v++) {
                adj[v] = new Bag<Edge>();
            }


            while ((line = bufferedReader.readLine()) != null) {
                String[] fields = line.split(" ");
                int v = Integer.parseInt(fields[1].trim());
                int w = Integer.parseInt(fields[2].trim());
                int weight = Integer.parseInt(fields[3].trim());
                Edge e = new Edge(v, w, weight);
                addEdge(e);
                //System.out.println(line);
            }
            bufferedReader.close();
        } catch (FileNotFoundException ex) {
            System.out.println("Unable to open file");
        } catch (IOException ex) {
            System.out.println("Error reading file");
        }
    }

    public EdgeWeightedGraph(In in) {
        String line = null;
        if (in == null)
            throw new IllegalArgumentException("argument is null");
        try {
            this.V = in.readInt();
            if (V < 0)
                throw new IllegalArgumentException("number of vertices in a Digraph must be non-negative");
            adj = (Bag<Edge>[]) new Bag[V + 1];
            inc = (Bag<Edge>[]) new Bag[V + 1];
            for (int v = 1; v <= V; v++) {
                adj[v] = new Bag<Edge>();
                inc[v] = new Bag<Edge>();
            }

            int E = in.readInt();
            if (E < 0)
                throw new IllegalArgumentException("Number of edges must be non-negative");
            for (int i = 0; i < E; i++) {
                int v = in.readInt();
                int w = in.readInt();
                int weight = in.readInt();
                addEdge(new Edge(v, w, weight));
            }

        } catch (NoSuchElementException e) {
            throw new IllegalArgumentException("invalid input format in EdgeWeightedDigraph constructor", e);
        }
    }

    public int V() {
        return V;
    }

    public int E() {
        return E;
    }

    public void addEdge(Edge e) {
        int v = e.either();
        int w = e.other(v);
        adj[v].add(e);       // outgoing
        inc[w].add(e);      // incoming    s ---  v --> w --- t

        //adj[w].add(e);          // !!!!
        E++;
    }

    public Iterable<Edge> adj(int v) {
        return adj[v];
    }

    public Iterable<Edge> inc(int v) {
        return inc[v];
    }

    private void loadGraph() {
        String line = null;
        //Resource res = new ClassPathResource("conf/mt_1_gr.txt");
        Resource res = new ClassPathResource("conf/melb_3_gr.txt");

        try {
            File file = res.getFile();
            FileReader fileReader = new FileReader(file);
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            this.V = Integer.parseInt(bufferedReader.readLine().trim());
            this.E = Integer.parseInt(bufferedReader.readLine().trim());

            adj = (Bag<Edge>[]) new Bag[V + 1];
            inc = (Bag<Edge>[]) new Bag[V + 1];
            for (int v = 1; v <= V; v++) {
                adj[v] = new Bag<Edge>();
                inc[v] = new Bag<Edge>();
            }

            while ((line = bufferedReader.readLine()) != null) {
                String[] fields = line.split(" ");
                int v = Integer.parseInt(fields[1].trim());
                int w = Integer.parseInt(fields[2].trim());
                int weight = Integer.parseInt(fields[3].trim());
                Edge e = new Edge(v, w, weight);
                addEdge(e);
            }
            bufferedReader.close();
        } catch (FileNotFoundException ex) {
            System.out.println("Unable to open file");
        } catch (IOException ex) {
            System.out.println("Error reading file");
        }
    }

    private void loadCoordinates() {
        //Resource res = new ClassPathResource("conf/mt_1_co.txt");
        Resource res = new ClassPathResource("conf/melb_3_co.txt");
        String line = null;

        /**
         * Initial Region
         */
        int renIdx;
        //double left = 144.8898013, right = 145.2419987, top = -37.7593004, bottom = -37.9544996;        // Testing

        double left = 144.7435007, right = 145.3614997, top = -37.6490007, bottom = -38.2683988;      // Online
        double vertical = (top - bottom) / rNum;
        double horizontal = (right - left) / rNum;
        verArr = new double[rNum + 1];
        horArr = new double[rNum + 1];
        for (int i = 0; i <= rNum; i++) {
            verArr[i] = bottom + vertical * i;
            horArr[i] = left + horizontal * i;
        }
        tiles = new ArrayList<List<Integer>>(rNum * rNum);
        for (int i = 0; i < rNum * rNum; i++) {
            tiles.add(new ArrayList<Integer>());
        }

        /**
         * Reading nodes
         */
        coo = new LatLng[V + 1];
        coo[0] = new LatLng(-1.0, 1.0);
        try {
            File file = res.getFile();
            FileReader fileReader = new FileReader(file);
            BufferedReader bufferedReader = new BufferedReader(fileReader);

            int k = 1;
            while ((line = bufferedReader.readLine()) != null) {
                String[] fields = line.trim().split(" ");

                renIdx = getRenIdx(Double.parseDouble(fields[2]), Double.parseDouble(fields[3]));
                tiles.get(renIdx).add(k);

                coo[k] = new LatLng(Double.parseDouble(fields[2]), Double.parseDouble(fields[3]));
                k++;
            }
            bufferedReader.close();
        } catch (FileNotFoundException ex) {
            System.out.println("Unable to open file");
        } catch (IOException ex) {
            System.out.println("Error reading file");
        }
    }

    public LatLng getCoordinate(int v) {
        return coo[v];
    }

    public String getCooString(int v) {
        return coo[v].getLat() + "_" + coo[v].getLng();
    }

    public int getNearestNode(String latLng) {
        String[] lalo = latLng.trim().split("_");
        double distance = Double.POSITIVE_INFINITY;
        double currentDis = 0.0;
        int nodeId = 0;

        int rIdx = getRenIdx(Double.parseDouble(lalo[0]), Double.parseDouble(lalo[1]));
        List<Integer> subTiles = tiles.get(rIdx);

        if (subTiles.isEmpty()) {
            // Optimise this code
            int fIdx = rIdx + 1;
            while (fIdx < tiles.size()) {
                if (!tiles.get(fIdx).isEmpty()) {
                    subTiles = tiles.get(fIdx);
                    break;
                }
                fIdx++;
            }
            if (subTiles.isEmpty()) {
                int bIdx = rIdx - 1;
                while (bIdx > 0) {
                    if (!tiles.get(bIdx).isEmpty()) {
                        subTiles = tiles.get(bIdx);
                        break;
                    }
                    bIdx--;
                }
            }
        }

        for (Integer i : subTiles) {
            currentDis = getEDist(Double.parseDouble(lalo[0]), Double.parseDouble(lalo[1]), coo[i].getLat(), coo[i].getLng());
            if (currentDis < distance) {
                distance = currentDis;
                nodeId = i;
            }
        }

        return nodeId;
    }

    public PriorityQueue<Node> getKNN(String latLng, int n) {
        int currentDis = 0;
        String[] lalo = latLng.trim().split("_");
        PriorityQueue<Node> pq = new  PriorityQueue<>();

        int rIdx = getRenIdx(Double.parseDouble(lalo[0]), Double.parseDouble(lalo[1]));
        List<Integer> subTiles = tiles.get(rIdx);

        if (subTiles.isEmpty()) {
            // Optimise this code
            int fIdx = rIdx + 1;
            while (fIdx < tiles.size()) {
                if (!tiles.get(fIdx).isEmpty()) {
                    subTiles = tiles.get(fIdx);
                    break;
                }
                fIdx++;
            }
            if (subTiles.isEmpty()) {
                int bIdx = rIdx - 1;
                while (bIdx > 0) {
                    if (!tiles.get(bIdx).isEmpty()) {
                        subTiles = tiles.get(bIdx);
                        break;
                    }
                    bIdx--;
                }
            }
        }

        for (Integer i : subTiles) {
            currentDis = (int)getEDist(Double.parseDouble(lalo[0]), Double.parseDouble(lalo[1]), coo[i].getLat(), coo[i].getLng());
            if (pq.size() < n) {
                pq.add(new Node(currentDis, i));
            } else {
                assert pq.peek() != null;
                if (currentDis < pq.peek().getK()) {
                    pq.poll();
                    pq.add(new Node(currentDis, i));
                }
            }
        }
        return pq;
    }

    private int getRenIdx(double lat, double lng) {
        int verIdx = 0, horIdx = 0;
        for (int i = 0; i <= rNum; i++)
            if (Double.compare(lat, verArr[i]) <= 0) {
                verIdx = i == 0 ? 0 : (i - 1);
                break;
            }
        for (int i = 0; i <= rNum; i++)
            if (Double.compare(lng, horArr[i]) <= 0) {
                horIdx = i == 0 ? 0 : (i - 1);
                break;
            }
        //return horIdx * rNum + verIdx;
        return verIdx * rNum + horIdx;
    }

    private double getEDist(double lat1, double lon1, double lat2, double lon2) {
        double theta = lon1 - lon2;
        double dist = Math.sin(deg2rad(lat1)) * Math.sin(deg2rad(lat2)) + Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) * Math.cos(deg2rad(theta));
        dist = Math.acos(dist);
        dist = rad2deg(dist);
        dist = dist * 60 * 1.1515;
        dist = dist * 1.609344;

        //return (dist);             // return Kilometer
        return (dist * 10000);       // COL data
        //return (dist * 100000);
    }

    // This function converts decimal degrees to radians
    private double deg2rad(double deg) {
        return (deg * Math.PI / 180.0);
    }

    // This function converts radians to decimal degrees
    private double rad2deg(double rad) {
        return (rad * 180 / Math.PI);
    }

    public static void main(String[] args) {
    }
}
