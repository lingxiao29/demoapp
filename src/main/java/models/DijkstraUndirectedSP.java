package models;

import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class DijkstraUndirectedSP {
    private int stDist;
    private int[] distTo;          // distTo[v] = distance  of shortest s->v path
    private Edge[] edgeTo;            // edgeTo[v] = last edge on shortest s->v path
    private IndexMinPQ<Integer> pq;
    private Stack<Integer> spTree = new Stack<Integer>();

    public DijkstraUndirectedSP(EdgeWeightedGraph G, int s) {
        distTo = new int[G.V() + 1];
        edgeTo = new Edge[G.V() + 1];
        for (int v = 0; v <= G.V(); v++)
            distTo[v] = Integer.MAX_VALUE;
        distTo[s] = 0;

        edgeTo[s] = new Edge(Integer.MAX_VALUE, s, 0);
        pq = new IndexMinPQ<Integer>(G.V() + 1);
        pq.insert(s, distTo[s]);
        while (!pq.isEmpty()) {
            int v = pq.delMin();
            spTree.push(v);
            for (Edge e : G.adj(v))
                relax(e, v);
        }
    }

    public DijkstraUndirectedSP(EdgeWeightedGraph G, int s, int t) {
        stDist = 0;
        distTo = new int[G.V() + 1];
        edgeTo = new Edge[G.V() + 1];
        for (int v = 0; v <= G.V(); v++)
            distTo[v] = Integer.MAX_VALUE;
        distTo[s] = 0;

        edgeTo[s] = new Edge(Integer.MAX_VALUE, s, 0);
        pq = new IndexMinPQ<Integer>(G.V() + 1);
        pq.insert(s, distTo[s]);
        while (!pq.isEmpty()) {
            int v = pq.delMin();
            if (v == t) {
                stDist = distTo[v];
                break;
            }
            for (Edge e : G.adj(v))
                relax(e, v);
        }
    }

    // get forward spt
    public DijkstraUndirectedSP(EdgeWeightedGraph G, int s, int t, double ub) {
        stDist = 0;
        distTo = new int[G.V() + 1];
        edgeTo = new Edge[G.V() + 1];
        for (int v = 0; v <= G.V(); v++)
            distTo[v] = Integer.MAX_VALUE;
        distTo[s] = 0;

        edgeTo[s] = new Edge(Integer.MAX_VALUE, s, 0);
        pq = new IndexMinPQ<Integer>(G.V() + 1);
        pq.insert(s, distTo[s]);
        while (!pq.isEmpty()) {
            int v = pq.delMin();

            if (v == t) {
                stDist = distTo[v];
            }

            if (stDist == 0) {
            //if (distTo[v] < spDistance * 1.5) {
                spTree.push(v);
                for (Edge e : G.adj(v))
                    relax(e, v);
            } else if (stDist != 0 && distTo[v] < stDist * ub) {
                spTree.push(v);
                for (Edge e : G.adj(v))
                    relax(e, v);
            }
        }
    }

    // get backward spt
    public DijkstraUndirectedSP(EdgeWeightedGraph G, int s, DijkstraUndirectedSP spt) {
        distTo = new int[G.V() + 1];
        edgeTo = new Edge[G.V() + 1];
        for (int v = 1; v <= G.V(); v++)                                        // 0 or 1
            distTo[v] = Integer.MAX_VALUE;
        distTo[s] = 0;

        edgeTo[s] = new Edge(Integer.MAX_VALUE, s, 0);
        pq = new IndexMinPQ<Integer>(G.V() + 1);
        pq.insert(s, distTo[s]);
        while (!pq.isEmpty()) {
            int v = pq.delMin();

            //if (distTo[v] + G.getEuclidean(t, v) <= spDistance * 2) {
            if (distTo[v] + spt.distTo[v] < spt.distTo[s] * 1.4) {
                spTree.push(v);
                for (Edge e : G.adj(v))
                    relax(e, v);
            }
        }
    }

    public DijkstraUndirectedSP(EdgeWeightedGraph G, int t, DijkstraUndirectedSP spt, boolean reverse, double ub) {
        distTo = new int[G.V() + 1];
        edgeTo = new Edge[G.V() + 1];
        for (int v = 1; v <= G.V(); v++)                                        // 0 or 1
            distTo[v] = Integer.MAX_VALUE;
        distTo[t] = 0;

        edgeTo[t] = new Edge(Integer.MAX_VALUE, t, 0);
        pq = new IndexMinPQ<Integer>(G.V() + 1);
        pq.insert(t, distTo[t]);

        while (!pq.isEmpty()) {
            int v = pq.delMin();
            if (distTo[v] + spt.distTo[v] < spt.distTo[t] * ub) {
                spTree.push(v);
                for (Edge e : G.inc(v))  // incoming edges
                    reverseLax(e, v);
            }
        }
    }

    private void relax(Edge e, int v) {
        int w = e.other(v);
        if (distTo[w] > distTo[v] + e.weight()) {
            distTo[w] = distTo[v] + e.weight();
            edgeTo[w] = e;
            if (pq.contains(w))
                pq.decreaseKey(w, distTo[w]);
            else
                pq.insert(w, distTo[w]);
        }
    }

    private void reverseLax(Edge e, int v) {
        int w = e.either();
        if (distTo[w] > distTo[v] + e.weight()) {
            distTo[w] = distTo[v] + e.weight();
            edgeTo[w] = e;
            if (pq.contains(w))
                pq.decreaseKey(w, distTo[w]);
            else
                pq.insert(w, distTo[w]);
        }
    }

    public int distTo(int v) {
        return distTo[v];
    }

    public boolean hasPathTo(int v) {
        return distTo[v] < Integer.MAX_VALUE;
    }

    public Edge getEdge(int v) {
        return edgeTo[v];
    }

    public int stDistance() {
        return stDist;
    }

    public Stack<Integer> getSpTree() {
        return spTree;
    }

    public static void main(String[] args) {
//        // Construct the graph; need to include the coordinates
        In in = new In("src/main/webapp/WEB-INF/classes/conf/tiny_tests.txt");
        EdgeWeightedGraph tg = new EdgeWeightedGraph(in);

        // Create forward and backward shortest path tree
        DijkstraUndirectedSP spt = new DijkstraUndirectedSP(tg, 1);
        DijkstraUndirectedSP tpt = new DijkstraUndirectedSP(tg, 2);

        // If a vertex is visited
        boolean[] visited = new boolean[tg.V() + 1];

        Stack<Integer> treeNodes = tpt.getSpTree();
        // a plateau pl(u, v); s-->u-->v-->t; v is tail
        double depth;
        int vid, pid, tail, head, plNodes = 1;
        while(!treeNodes.isEmpty()) {
            vid = treeNodes.pop();
            tail = head = vid;
            if (!visited[vid]) {
                visited[vid] = true;
                pid = tpt.getEdge(vid).either();
                while (pid != Integer.MAX_VALUE && !visited[pid]) {
                    if (spt.getEdge(pid).either() == vid) {
                        visited[pid] = true;
                        plNodes++;
                        if (plNodes == 2) {
                            tail = vid;
                            head = pid;
                        } else {
                            head = pid;
                        }
                        vid = pid;
                        pid = tpt.getEdge(vid).either();
                    } else
                        break;
                }
                if (plNodes >= 2) {
                    depth = tpt.distTo(tail) - tpt.distTo(head);
                    StdOut.println(tail + " --> " + head + " = " + depth);
                }
            }
            plNodes = 1;
        }
    }
}