package models;

public class Node implements Comparable<Node> {
    private int k;
    private int v;

    public Node() {
        this.k = 0;
        this.v = 0;
    }

    public Node(int key, int value) {
        this.k = key;
        this.v = value;
    }

    public int getK() {
        return k;
    }

    public int getV() {
        return v;
    }

    @Override
    // Increasing order
    public int compareTo(Node that) {
        return Integer.valueOf(this.k).compareTo(that.k);
        /*if (this.k < that.k)
            return -1;
        else if (this.k > that.k)
            return 1;
        else
            return 0;*/
    }

    // Decreasing order
    /*public int compareTo(Node that) {
        //return Double.compare(that.k, this.k);
        return Integer.valueOf(that.k).compareTo(this.k);
    }*/

    public String toString() {
        return String.format("%d-%d", k, v);
    }
}
