package models;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

public class Testing {
    public static void main(String[] args) throws IOException {
        String text = "Text to save to file\t";
        List<String> list1 = Arrays.asList("Line 1", "Line 2");
        Files.write(Paths.get("C:/Users/lli278/Downloads/alternative/log.txt"), text.getBytes(), StandardOpenOption.CREATE, StandardOpenOption.APPEND);
        Files.write(Paths.get("C:/Users/lli278/Downloads/alternative/log.txt"), list1, StandardOpenOption.CREATE, StandardOpenOption.APPEND);

        List<Double> list2 = Arrays.asList(1.222222, 2.222222, 3.33333);
        String delim = "-";
        String res = list2.stream().map(Math::round).map(Object::toString)
                .collect(Collectors.joining(delim));

        System.out.println(res);

        StringBuilder sb = new StringBuilder();
        sb.append(12).append(",").append(23).append(",comment").append(",").append(res);
        StdOut.println(sb.toString());

        double a = 3.222222;
        StdOut.println((int)Math.round(a));

        String timeStamp = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").format(Calendar.getInstance().getTime());
        StdOut.println(timeStamp);

        HashMap<String, Integer> speeds = new HashMap<String, Integer>();
        speeds.put("motorway", 100);
        speeds.put("motorway_link", 45);
        speeds.put("trunk", 80);
        speeds.put("trunk_link", 40);
        speeds.put("primary", 60);
        speeds.put("primary_link", 30);
        speeds.put("secondary", 50);
        speeds.put("secondary_link", 25);
        speeds.put("tertiary", 40);
        speeds.put("tertiary_link", 20);
        speeds.put("unclassified", 20);
        speeds.put("residential", 20);
        speeds.put("living_street", 10);
        speeds.put("service", 10);

        String x = "motorway_link";
        String y = "motorway_link";

        if (x.equals("motorway_link"))
            StdOut.println(x.equals("motorway_link") + " " + speeds.get(y));
    }
}
