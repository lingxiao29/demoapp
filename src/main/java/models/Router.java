package models;

import java.util.*;

public class Router {
    private int s;
    private int t;
    private int TK;
    private double UB;
    private double PNT;
    private double SIM;
    private double DM;
    private EdgeWeightedGraph TG;
    private DijkstraUndirectedSP spt;
    private DijkstraUndirectedSP tpt;

    private PriorityQueue<Node> viaNodes = new PriorityQueue<Node>();


    public Router(int s, int t, EdgeWeightedGraph G, DijkstraUndirectedSP spt, DijkstraUndirectedSP tpt, int tk, double ub, double pen, double sim, double dm) {
        this.s = s;
        this.t = t;
        this.TG = G;
        this.spt = spt;
        this.tpt = tpt;
        this.TK = tk;
        this.UB = ub;
        this.PNT = pen;
        this.SIM = sim;
        this.DM = dm;
    }

//    private List<Edge> getNewPlateaus(int k) {
//        boolean[] visited = new boolean[TG.V() + 1];
//        Stack<Integer> treeNodes = tpt.getSpTree();
//        PriorityQueue<Edge> pls = new PriorityQueue<Edge>();
//        List<Edge> topKPls = new ArrayList<Edge>();
//
//        int depth;
//        int vid, pid, tail, head, plNodes = 1;
//        while (!treeNodes.isEmpty()) {
//            vid = treeNodes.pop();
//            if (spt.distTo(vid) != Integer.MAX_VALUE && spt.distTo(vid) + tpt.distTo(vid) <= spt.distTo(t) * UB) {     // Need to be improved search space not here
//                if (spt.distTo(vid) == tpt.distTo(vid) + spt.distTo(t) || tpt.distTo(vid) == spt.distTo(vid) + spt.distTo(t)) {
//                    visited[vid] = true;
//                    continue;
//                }
//                tail = head = vid;
//                if (!visited[vid]) {
//                    visited[vid] = true;
//                    pid = tpt.getEdge(vid).other(vid);
//                    while (pid != Integer.MAX_VALUE && !visited[pid]) {
//                        if (spt.getEdge(pid) != null && spt.getEdge(pid).either() == vid) {
//                            visited[pid] = true;
//                            plNodes++;
//                            if (plNodes == 2) {
//                                tail = vid;
//                                head = pid;
//                            } else {
//                                head = pid;
//                            }
//                            vid = pid;
//                            pid = tpt.getEdge(vid).other(vid);
//                        } else
//                            break;
//                    }
//                    if (plNodes >= 2) {
//                        depth = tpt.distTo(tail) - tpt.distTo(head);
//                        pls.add(new Edge(tail, head, depth));
//                        // Add a compressed via-node to min-heap
//                        viaNodes.add(new Node(spt.distTo(tail) + tpt.distTo(tail), tail));
//                    } else
//                        // Add single via-node to min-heap
//                        viaNodes.add(new Node(spt.distTo(vid) + tpt.distTo(vid), vid));
//                }
//                plNodes = 1;
//            } else
//                visited[vid] = true;
//        }
//        while (!pls.isEmpty()) {
//            Edge e = pls.poll();
//            if (topKPls.size() < k)
//                topKPls.add(e);
//            else
//                break;
//        }
//        return topKPls;
//    }
//
//    //void getPaths(int k) {
//    public Map<Double, List<Integer>> getPaths() {
//        //List<Edge> kPLs = getPlateaus(TK);
//        List<Edge> kPLs = getNewPlateaus(TK);
//        double offset = 60;
//        double ofs = 2.2;
//        Map<Double, List<Integer>> topKPath = new TreeMap<Double, List<Integer>>();
//        for (Edge e : kPLs) {
//            List<Integer> path;
//            path = usRecovery(e.tail());                    // Recovery sub-path u -> s
//            Collections.reverse(path);                      // Reverse sub-path s -> u
//            path.addAll(u2tRecovery(e.tail()));
//            //path.addAll(utRecovery(e.tail()));            // Append sub-path u.p -> t; undirected graph
//
//            //double pathLen = ((spt.distTo(e.tail()) + tpt.distTo(e.tail()) + ofs / kPLs.size()));     // Testing
//            double pathLen = ((spt.distTo(e.tail()) + tpt.distTo(e.tail()) + offset)) / 60000;          // Online
//
//            topKPath.put(pathLen, path);
//            ofs++;
//            offset++;
//        }
//        return topKPath;
//    }

    public Map<Double, List<Integer>> getPaths() {
        boolean[] visited = new boolean[TG.V() + 1];
        Stack<Integer> treeNodes = tpt.getSpTree();
        PriorityQueue<Edge> pls = new PriorityQueue<Edge>();

        int depth;
        int vid, pid, tail, head, plNodes = 1;
        while (!treeNodes.isEmpty()) {
            vid = treeNodes.pop();
            if (spt.distTo(vid) != Integer.MAX_VALUE && spt.distTo(vid) + tpt.distTo(vid) <= spt.distTo(t) * UB) {     // Need to be improved search space not here
                if (spt.distTo(vid) == tpt.distTo(vid) + spt.distTo(t) || tpt.distTo(vid) == spt.distTo(vid) + spt.distTo(t)) {
                    visited[vid] = true;
                    continue;
                }
                tail = head = vid;
                if (!visited[vid]) {
                    visited[vid] = true;
                    pid = tpt.getEdge(vid).other(vid);
                    while (pid != Integer.MAX_VALUE && !visited[pid]) {
                        if (spt.getEdge(pid) != null && spt.getEdge(pid).either() == vid) {
                            visited[pid] = true;
                            plNodes++;
                            if (plNodes == 2) {
                                tail = vid;
                                head = pid;
                            } else {
                                head = pid;
                            }
                            vid = pid;
                            pid = tpt.getEdge(vid).other(vid);
                        } else
                            break;
                    }
                    if (plNodes >= 2) {
                        depth = tpt.distTo(tail) - tpt.distTo(head);
                        pls.add(new Edge(tail, head, depth));
                        // Add a compressed via-node to min-heap
                        viaNodes.add(new Node(spt.distTo(tail) + tpt.distTo(tail), tail));
                    } else
                        // Add single via-node to min-heap
                        viaNodes.add(new Node(spt.distTo(vid) + tpt.distTo(vid), vid));
                }
                plNodes = 1;
            } else
                visited[vid] = true;
        }

        /**
         * Top-k path recovery based on plateaus
         */
        int u;
        double pathLen = 0.0;
        List<Integer> sp;
        double offset = 60;
        double ofs = 2.2;
        Map<Double, List<Integer>> pathList = new TreeMap<Double, List<Integer>>();
        // take out sp
        if (!pls.isEmpty()) {
            u = pls.poll().tail();
            sp = pathRecovery(u);
            pathLen = ((spt.distTo(u) + tpt.distTo(u) + offset)) / 60000;
            //pathLen = ((spt.distTo(u) + tpt.distTo(u) + ofs / pathList.size()));
            pathList.put(pathLen, sp);
        }
        while (pathList.size() < TK && !pls.isEmpty()) {
            boolean isValid = true;
            u = pls.poll().tail();
            //pathLen = ((spt.distTo(u) + tpt.distTo(u) + ofs / pathList.size()));
            pathLen = ((spt.distTo(u) + tpt.distTo(u) + offset + 1)) / 60000;

            List<Integer> path;
            path = pathRecovery(u);                                             // get next path from u

            // Validation check for current p(u)
            for (Map.Entry<Double, List<Integer>> route : pathList.entrySet()) {
                int d = s, m = t;
                int xIdx = path.size() - 1;
                int yIdx = route.getValue().size() - 1;
                int size = Math.min(xIdx, yIdx);

                // get the deviation vertex
                for (int i = 0; i <= size; i++) {
                    if (!path.get(i).equals(route.getValue().get(i))) {
                        if (i >= 1) {
                            d = path.get(i -1);
                            break;
                        }
                    }
                }

                // get the merging vertex
                while(xIdx >=0 && yIdx >= 0) {
                    if (!path.get(xIdx).equals(route.getValue().get(yIdx))) {
                        m = path.get(xIdx + 1);
                        break;
                    }
                    xIdx--;
                    yIdx--;
                }

                if (m != d) {
                    int xLen = spt.distTo(u) - spt.distTo(d) + tpt.distTo(u) - tpt.distTo(m);
                    int yLen = Math.min((spt.distTo(m) - spt.distTo(d)), (tpt.distTo(d) - tpt.distTo(m)));   // this is wrong, need to get T for whole path
                    if (yLen <= 0) {
                        isValid = false;
                        break;
                    } else {
                        if ((xLen * 1.0 / yLen) >= this.DM) {
                            isValid = false;
                            break;
                        }
                    }
                } else {
                    isValid = false;
                    break;
                }
            }
            if (pathList.size() < TK) {
                if (isValid) {
                    pathList.put(pathLen, path);
                    ++ofs;
                    ++offset;
                }
            } else
                break;
        }
        return pathList;
    }

    public Map<Double, List<Integer>> simFunc() {
        int u;
        double pathLen = 0.0;
        List<Integer> sp;
        double offset = 60;
        double ofs = 2.2;
        Map<Double, List<Integer>> pathList = new TreeMap<Double, List<Integer>>();

        if (!viaNodes.isEmpty()) {
            u = viaNodes.poll().getV();
            sp = pathRecovery(u);
            //pathLen = ((spt.distTo(u) + tpt.distTo(u) + ofs / pathList.size()));
            pathLen = ((spt.distTo(u) + tpt.distTo(u) + offset)) / 60000;
            pathList.put(pathLen, sp);
        }

        while (pathList.size() < TK && !viaNodes.isEmpty()) {
            // Path from via-node heap
            boolean isValid = true;
            u = viaNodes.poll().getV();
            //pathLen = ((spt.distTo(u) + tpt.distTo(u) + ofs / pathList.size()));
            pathLen = ((spt.distTo(u) + tpt.distTo(u) + offset)) / 60000;

            List<Integer> path;
            path = pathRecovery(u);
            int midIndex = path.indexOf(u);
            boolean[] o = new boolean[TG.V() + 1];
            for (Integer i : path)
                o[i] = true;

            /*StdOut.print("via-node: " + u + "--> " + spt.distTo(u) + " " + tpt.distTo(u));
            for (Integer i : path)
                StdOut.print (i + "-");
            StdOut.println();*/

            for (Map.Entry<Double, List<Integer>> route : pathList.entrySet()) {
                int cn, ct = 0, head = 0, tail = 0, overlap = 0;
                List<Integer> rt = route.getValue();
                int n = rt.size();
                for (int j = 0; j < n; j++) {
                    cn = rt.get(j);
                    if (o[cn]) {
                        if (ct == 0)
                            tail = cn;
                        else
                            head = cn;
                        ct++;
                    } else {
                        if (ct > 1) {
                            if (path.indexOf(head) < midIndex)
                                overlap += spt.distTo(head) - spt.distTo(tail);
                            else
                                overlap += tpt.distTo(tail) - tpt.distTo(head);
                        }
                        if (ct != 0)
                            ct = head = tail = 0;
                    }
                }
                if (ct > 1)
                    overlap += tpt.distTo(tail) - tpt.distTo(head);

                double minLen = route.getKey() >= pathLen ? pathLen : route.getKey();
                //if ((overlap / minLen) > 0.5) {
                if ((overlap / (60000 * minLen)) > this.SIM) {               // Similarity threshold
                    isValid = false;
                    break;
                }
            }
            if (pathList.size() < TK) {
                if (isValid) {
                    pathList.put(pathLen, path);
                    ++ofs;
                    ++offset;
                }
            } else
                break;
        }
        return pathList;
    }

    public Map<Double, List<Integer>> penFunc() {
        double ofs = 2.2;
        double offset = 60;
        double pathLen;
        List<Integer> sp = new ArrayList<Integer>();
        EdgeWeightedGraph CG = new EdgeWeightedGraph(true);
        Map<Double, List<Integer>> pathList = new TreeMap<Double, List<Integer>>();
        // Take out the original sp

        //StdOut.println(s + " ," + t + " | " + spt.distTo(t) + " --- " + tpt.distTo(s));

        int u = t, v = t;
        while (u != s) {
            sp.add(u);
            v = u;
            u = spt.getEdge(u).either();
            for (Edge e : CG.adj(u)) {
                if (e.other(u) == v) {
                    e.setWeight((int) Math.ceil(e.weight() * PNT));
                }
            }
        }
        sp.add(s);

        //sp = pathRecovery(t);                                                   // 2020-06-16 added

        Collections.reverse(sp);
        pathList.put((spt.distTo(t) + offset) * 1.0 / 60000, sp);
        //pathList.put(spt.distTo(t) * 1.0, sp);

        while (pathList.size() < TK) {
            pathLen = 0.0;
            DijkstraUndirectedSP sptUpdate = new DijkstraUndirectedSP(CG, s, t);
            List<Integer> sps = new ArrayList<Integer>();
            u = v = t;
            while (u != s) {
                sps.add(u);
                v = u;
                u = sptUpdate.getEdge(u).either();
                for (Edge e : CG.adj(u)) {
                    if (e.other(u) == v) {
                        e.setWeight((int) Math.ceil(e.weight() * PNT));
                    }
                }
                for (Edge e : TG.adj(u)) {
                    if (e.other(u) == v) {
                        pathLen += e.weight();
                    }
                }
            }
            if (pathLen > spt.distTo(t) * UB)                                   // Checking the upper bound
                break;

            sps.add(s);
            Collections.reverse(sps);

            boolean isInsert = true;
            for (List<Integer> pp : pathList.values()) {
                if (pp.equals(sps)) {
                    isInsert = false;
                    break;
                }
            }
            if (isInsert)   // Need to check UB
                //pathList.put(sptUpdate.distTo(t) + ofs / (pathList.size() + 1), sps);
                pathList.put((pathLen + offset) * 1.0 / 60000, sps);

            ofs++;
            offset++;
        }

        return pathList;
    }

    private List<Integer> pathRecovery(int u) {
        List<Integer> p;
        p = usRecovery(u);
        Collections.reverse(p);
        p.addAll(u2tRecovery(u));
        return p;
    }

    private List<Integer> usRecovery(int u) {
        List<Integer> path = new ArrayList<Integer>();
        int n = u;
        while (n != Integer.MAX_VALUE) {
            path.add(n);
            n = spt.getEdge(n).either();
        }
        return path;
    }

    private List<Integer> utRecovery(int u) {
        List<Integer> path = new ArrayList<Integer>();
        int n = tpt.getEdge(u).either();
        while (n != Integer.MAX_VALUE) {
            path.add(n);
            n = tpt.getEdge(n).either();
        }
        return path;
    }

    private List<Integer> u2tRecovery(int u) {
        List<Integer> path = new ArrayList<Integer>();
        int n = tpt.getEdge(u).other(u);
        while (n != Integer.MAX_VALUE) {
            path.add(n);
            n = tpt.getEdge(n).other(n);
        }
        return path;
    }

    public static void main(String[] args) {
        // Construct the graph; need to include the coordinates
        In in = new In("src/main/webapp/WEB-INF/classes/conf/tiny_tests.txt");
        EdgeWeightedGraph tg = new EdgeWeightedGraph(in);

        // Create forward and backward shortest path tree
        double ub = 1.4;
        int s = 1, t = 2;
        DijkstraUndirectedSP spt = new DijkstraUndirectedSP(tg, s, t, ub);
        DijkstraUndirectedSP tpt = new DijkstraUndirectedSP(tg, t, spt, true, ub);

        // get top-k plateaus
        Router pls = new Router(s, t, tg, spt, tpt, 5, ub, 1.2, 0.5, 1.5);

        StdOut.println(tg.V());

        // Get k-plateaus
        //       List<Edge> kPLs = pls.getNewPlateaus(5);
//        for (Edge e : kPLs) {
//            StdOut.println(e.toString());
//        }

        // Get plateau-paths
        Map<Double, List<Integer>> topKPath = pls.getPaths();
        StdOut.println("Plateau path: " + topKPath.size());
        for (List<Integer> pp : topKPath.values()) {
            for (int p : pp) {
                StdOut.print(p + " ");
            }
            StdOut.println();
        }

//        // get sim-paths
//        Map<Double, List<Integer>> simKPath = pls.simFunc(5);
//        StdOut.println("Sim-path:" + simKPath.size());
//        for (List<Integer> pp : simKPath.values()) {
//            for (int p : pp) {
//                StdOut.print(p + " ");
//            }
//            StdOut.println();
//        }

        /*DijkstraUndirectedSP penTree = new DijkstraUndirectedSP(tg, s, t);
        StdOut.println("s to t dist: " + penTree.distTo(t));
        List<Integer> sp = new ArrayList<Integer>();
        int u = t, v = t;
        while (u != s) {
            sp.add(u);
            v = u;
            u = penTree.getEdge(u).either();
            StdOut.print(u + "-" + v + "->");    // 2147483647 - 1
            // Change edge weight in G: e = {u -> v}
            for (Edge e : tg.adj(u)) {
                if (e.other(u) == v) {
                    StdOut.println(e.toString());
                    e.setWeight((int)Math.ceil(e.weight() * 1.1));
                }
            }
        }
        sp.add(s);
        for (Integer i : sp)
            StdOut.print(i + " ");
        StdOut.println();

        DijkstraUndirectedSP penTree2 = new DijkstraUndirectedSP(tg, s, t);
        StdOut.println("s to t dist: " + penTree2.distTo(t));
        List<Integer> sp2 = new ArrayList<Integer>();
        u = v = t;
        while (u != s) {
            sp2.add(u);
            v = u;
            u = penTree2.getEdge(u).either();
            StdOut.print(u + "-" + v + "->");    // 2147483647 - 1
            // Change edge weight in G: e = {u -> v}
            for (Edge e : tg.adj(u)) {
                if (e.other(u) == v) {
                    StdOut.println(e.toString());
                    e.setWeight((int)Math.ceil(e.weight() * 1.1));
                }
            }
        }
        sp2.add(s);
        for (Integer i : sp2)
            StdOut.print(i + " ");
        StdOut.println();*/

        // Penalty
//        Map<Double, List<Integer>> topKPath = pls.penFunc();
//        StdOut.println("Plateau path: " + topKPath.size());
//        for (List<Integer> pp : topKPath.values()) {
//            for (int p : pp) {
//                StdOut.print(p + " ");
//            }
//            StdOut.println();
//        }

//        int rNum = 20;
//        double left = 144.8898013, right = 145.2419987, top = -37.7593004, bottom = -37.9544996;
//        double vertical = (top - bottom) / 20;
//        double horizontal = (right - left) / 20;
//        StdOut.println(vertical + " " + horizontal);
//
//        double[] verArr = new double[rNum + 1];
//        double[] horArr = new double[rNum + 1];
//
//        for (int i = 0; i <= rNum; i++) {
//            verArr[i] = bottom + vertical * i;
//            horArr[i] = left + horizontal * i;
//        }
//
//        for (Double d : verArr) {
//            StdOut.print(d + " ");
//        }
//        StdOut.println();
//        for (Double d : horArr) {
//            StdOut.print(d + " ");
//        }
//        StdOut.println();
//
//        List<List<Integer>> tiles = new ArrayList<List<Integer>>(rNum * rNum);
//        for (int i = 0; i <rNum * rNum; i++) {
//            tiles.add(new ArrayList<Integer>());
//        }
//
//        int verIdx = 0, horIdx = 0;
//        for (int i = 0; i <= rNum; i++) {
//            if (Double.compare(-37.9544996, verArr[i]) <= 0) {
//                verIdx = i == 0 ? 0 : (i - 1);
//                StdOut.println(i + ", " + verIdx);
//                break;
//            }
//        }
//
//        for (int i = 0; i <= rNum; i++) {
//            if (Double.compare(144.8898013, horArr[i]) <= 0) {
//                horIdx = i == 0 ? 0 : (i - 1);
//                StdOut.println(i + ", " + horIdx);
//                break;
//            }
//        }
//
//        tiles.get(horIdx * rNum + verIdx).add(10);
//        StdOut.println(tiles.get(horIdx * rNum + verIdx + 1).size());
    }
}