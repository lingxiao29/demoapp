package controllers;

import models.*;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

@Controller
public class IndexController {

    private int ss;
    private int tt;
    private LatLng ssCor;
    private LatLng ttCor;

    private List<Double> goLength;
    private List<Double> plLength;
    private List<Double> simLength;
    private List<Double> penLength;

    private final EdgeWeightedGraph G;

    public IndexController() {
        G = new EdgeWeightedGraph();
    }

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public ModelAndView showForm() {
//        String[] dmList = {"2.5", "3.5", "4.5", "5.5"};
//        return new ModelAndView("index", "parameter", new Parameter(dmList));
        return new ModelAndView("index", "parameter", new Parameter());
    }

    @RequestMapping(value = "/plotroute", method = RequestMethod.POST)
    public ModelAndView plotRoutes(@ModelAttribute("parameter") Parameter parameter, ModelMap model) {
        // tiles
        int s = G.getNearestNode(parameter.getSource());
        int t = G.getNearestNode(parameter.getTarget());

        int TK = 3;
        double UB = 1.4;
        double PEN = 1.4;
        double SIM = 0.5;
        double DM = 4;

        DijkstraUndirectedSP spt = new DijkstraUndirectedSP(G, s, t, UB);
        DijkstraUndirectedSP tpt = new DijkstraUndirectedSP(G, t, spt, true, UB);

        if (spt.distTo(t) != Integer.MAX_VALUE && tpt.distTo(s) != Integer.MAX_VALUE) {

            // Google Maps
            double offset = 60;
            // g routes
            //List<Integer> gRoute = new ArrayList<>();
            List<PriorityQueue<Node>> gRoutes = new ArrayList<>();
            List<Double> gLabels = new ArrayList<>();

            if (!parameter.getRx().isEmpty()) {
                //gRoute.add(G.getNearestNode(parameter.getRx()));
                gRoutes.add(G.getKNN(parameter.getRx(), 3));
            }
            if (!parameter.getRy().isEmpty()) {
                //gRoute.add(G.getNearestNode(parameter.getRy()));
                gRoutes.add(G.getKNN(parameter.getRy(), 3));
            }
            if (!parameter.getRz().isEmpty()) {
                //gRoute.add(G.getNearestNode(parameter.getRz()));
                gRoutes.add(G.getKNN(parameter.getRz(), 3));
            }
//        for (Integer u : gRoute) {
//            DijkstraUndirectedSP su = new DijkstraUndirectedSP(G, s, u);
//            DijkstraUndirectedSP us = new DijkstraUndirectedSP(G, u, s);
//
//            int suDist = Math.min(su.stDistance(), us.stDistance());
//
//            DijkstraUndirectedSP ut = new DijkstraUndirectedSP(G, u, t);
//            DijkstraUndirectedSP tu = new DijkstraUndirectedSP(G, t, u);
//
//            int utDist = Math.min(ut.stDistance(), tu.stDistance());
//
//            gLabels.add((suDist + utDist + offset) * 1.0 / 60000);
//            offset++;
//        }

            for (PriorityQueue<Node> pq : gRoutes) {
                int length = Integer.MAX_VALUE;
                while (!pq.isEmpty()) {
                    int u = pq.poll().getV();
                    DijkstraUndirectedSP su = new DijkstraUndirectedSP(G, s, u);
                    DijkstraUndirectedSP ut = new DijkstraUndirectedSP(G, u, t);

                    if ((su.stDistance() + ut.stDistance()) < length)
                        length = su.stDistance() + ut.stDistance();
                }
                gLabels.add((length + offset) * 1.0 / 60000);
                offset++;
            }

//        DijkstraUndirectedSP spt = new DijkstraUndirectedSP(G, s, t, UB);
//        DijkstraUndirectedSP tpt = new DijkstraUndirectedSP(G, t, spt, true, UB);
            Router pl = new Router(s, t, G, spt, tpt, TK, UB, PEN, SIM, DM);

            // Plateau
            List<List<Position>> plRoutes = new ArrayList<List<Position>>();
            List<Double> plLabels = new ArrayList<Double>();
            for (Map.Entry<Double, List<Integer>> entry : pl.getPaths().entrySet()) {
                plLabels.add(entry.getKey());
                List<Position> positions = new ArrayList<Position>();
                for (int i : entry.getValue()) {
                    Position p = new Position(G.getCoordinate(i).getLat(), G.getCoordinate(i).getLng());
                    positions.add(p);
                }
                plRoutes.add(positions);
            }

            // Similarity
            List<List<Position>> simRoutes = new ArrayList<List<Position>>();
            List<Double> simLabels = new ArrayList<Double>();
            for (Map.Entry<Double, List<Integer>> entry : pl.simFunc().entrySet()) {
                simLabels.add(entry.getKey());
                List<Position> positions = new ArrayList<Position>();
                for (int i : entry.getValue()) {
                    Position p = new Position(G.getCoordinate(i).getLat(), G.getCoordinate(i).getLng());
                    positions.add(p);
                }
                simRoutes.add(positions);
            }

            // Penalty
            List<List<Position>> penRoutes = new ArrayList<List<Position>>();
            List<Double> penLabels = new ArrayList<Double>();
            for (Map.Entry<Double, List<Integer>> entry : pl.penFunc().entrySet()) {
                penLabels.add(entry.getKey());
                List<Position> positions = new ArrayList<Position>();
                for (int i : entry.getValue()) {
                    Position p = new Position(G.getCoordinate(i).getLat(), G.getCoordinate(i).getLng());
                    positions.add(p);
                }
                penRoutes.add(positions);
            }

            model.addAttribute("source", G.getCooString(s));
            model.addAttribute("target", G.getCooString(t));
            model.addAttribute("plLabels", plLabels);
            model.addAttribute("plRoutes", plRoutes);
            model.addAttribute("simLabels", simLabels);
            model.addAttribute("simRoutes", simRoutes);
            model.addAttribute("penLabels", penLabels);
            model.addAttribute("penRoutes", penRoutes);
            model.addAttribute("gLabels", gLabels);

            // Survey page
            String[] mList = {"Yes", "No"};
            String[] rtList = {"1", "2", "3", "4", "5"};
            model.addAttribute("mList", mList);
            model.addAttribute("rtList", rtList);

            // Construct log
            this.ss = s;
            this.tt = t;
            this.ssCor = G.getCoordinate(s);
            this.ttCor = G.getCoordinate(t);
            this.goLength = gLabels;
            this.plLength = plLabels;
            this.simLength = simLabels;
            this.penLength = penLabels;
            // Construct log

            //return "plotroute";
            return new ModelAndView("plotroute");
        } else {
            return new ModelAndView("index", "parameter", new Parameter("Yes"));
        }
    }

    @RequestMapping(value = "submitSurvey", method = RequestMethod.POST)
    //public @ResponseBody boolean getSurvey(@RequestBody Parameter p) throws IOException {
    public @ResponseBody String getSurvey(Parameter p) throws IOException {
        String delim = "-";
        String go = this.goLength.stream().map(Math::round).map(Object::toString).collect(Collectors.joining(delim));
        String pl = this.plLength.stream().map(Math::round).map(Object::toString).collect(Collectors.joining(delim));
        String sim = this.simLength.stream().map(Math::round).map(Object::toString).collect(Collectors.joining(delim));
        String pen = this.penLength.stream().map(Math::round).map(Object::toString).collect(Collectors.joining(delim));

        String timeStamp = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").format(Calendar.getInstance().getTime());

        StringBuilder row = new StringBuilder();
        row.append(timeStamp).append(",").append(this.ss).append(",").append(this.tt).append(",").append(this.ssCor.toString()).append(",")
                .append(this.ttCor.toString()).append(",").append(p.getMel()).append(",").append(p.getGoRate()).append(",").append(p.getPlRate())
                .append(",").append(p.getSimRate()).append(",").append(p.getPenRate()).append(",").append(go).append(",").append(pl)
                .append(",").append(sim).append(",").append(pen).append(",").append(p.getComment()).append("\n");

        // DEV
        //Files.write(Paths.get("C:/Users/lli278/Downloads/alternative/dev_log.txt"), row.toString().getBytes(), StandardOpenOption.CREATE, StandardOpenOption.APPEND);

        // Online
        Files.write(Paths.get("/home/ubuntu/download/llx/demo_log.txt"), row.toString().getBytes(), StandardOpenOption.CREATE, StandardOpenOption.APPEND);

        return "Submit Successfully";
    }
}
