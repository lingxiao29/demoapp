<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<html>
<head>
    <style>
        /* Always set the map height explicitly to define the size of the div
         * element that contains the map. */
        #map {
            height: 100%;
        }

        /* Optional: Makes the sample page fill the window. */
        html, body {
            height: 100%;
            margin: 0;
            padding: 0;
        }

        #floating-panel {
            position: absolute;
            top: 3%;
            left: 1.5%;
            z-index: 5;
            /* width: 310px;
             height: 51px;*/
            width: 610px;
            height: 101px;
            border: 1px;
            /*border: 2px solid #999;*/
        }

        /* Style and fix the button on the page */
        .submit_button {
            background-color: #4A89F3;
            color: white;
            padding: 11px 20px;
            border: none;
            border-radius: 5px;
            cursor: pointer;
            position: absolute;
            left: 1%;
            /*font-size: large;*/
            font-size: xxx-large;
            outline-color: darkkhaki;
        }

        .clear_button {
            background-color: #4A89F3;
            color: white;
            padding: 11px 20px;
            border: none;
            border-radius: 5px;
            cursor: pointer;
            position: absolute;
            left: 36%;
            /*font-size: large;*/
            font-size: xxx-large;
            outline-color: darkkhaki;
        }

        .open-help {
            background-color: #4A89F3;
            color: white;
            padding: 11px 20px;
            border: none;
            border-radius: 5px;
            cursor: pointer;
            position: absolute;
            left: 70%;
            /*font-size: large;*/
            font-size: xxx-large;
            outline-color: darkkhaki;
        }

        #form-help {
            display: none;
            text-align: left;
            position: absolute;
            width: 360px;
            left: 20%;
            top:6%;
            z-index: 9;
            padding: 10px;
            opacity: 0.9;
            background-color: #FCFAFA;
        }
    </style>
</head>
<body>
<!--<h1 style="color:blue;"><b>Alternative Routes Demo</b></h1>-->
<div id="floating-panel">
    <form:form method="POST" action="/routeDemo/plotroute" modelAttribute="parameter" target="_blank">
        <table>
            <tr>
                <td style="padding-right:10px" hidden>
                    <form:label path="Source"><b style="color:green">Source:</b> </form:label>
                    <form:input id="source" path="source"/>
                </td>
                <td style="padding-right:10px" hidden>
                    <form:label path="Target"><b style="color:purple">Target:</b> </form:label>
                    <form:input id="target" path="target"/>
                </td>

                <td style="padding-right:10px" hidden>
                    <form:label path="Rx"><b style="color:purple">rx:</b> </form:label>
                    <form:input id="rx" path="rx"/>
                </td>
                <td style="padding-right:10px" hidden>
                    <form:label path="Ry"><b style="color:purple">ry:</b> </form:label>
                    <form:input id="ry" path="ry"/>
                </td>
                <td style="padding-right:10px" hidden>
                    <form:label path="Rz"><b style="color:purple">rz:</b> </form:label>
                    <form:input id="rz" path="rz"/>
                </td>

                <td><input class="submit_button" id="submit" onclick="validateForm()" type="submit" value="Submit"/></td>
                <td><input class="clear_button" onclick="clearPage()" type=button value=" Clear "></td>
                <td><input class="open-help" onclick="openHelp()" type=button value=" Help "></td>
            </tr>
        </table>
    </form:form>
</div>

<div id="map"></div>

<div id="form-help">
    <p>Click on the map anywhere to select two different locations.
        The first selected location will be the source and the second location will be the target.
        When you press "Submit", a new window will open which will display multiple routes
        from the source location to the target location. Press the "Clear" button to clear
        the selected locations.</p>
    <button type="button" onclick="closeHelp()">Close</button>
</div>


<script>
    // Online
    var bounds = {
        north: -37.6490007,
        south: -38.2683988,
        west: 144.7435007,
        east: 145.3614997
    };
    /**
     * Create new map
     */
    var map;
    var myMapOptions = {
        zoom: 12,                                           // Col: 8; Mel 10
        center: {lat: -37.826844, lng: 145.060886},       // Mel -37.826844, 145.060886
        mapTypeId: 'roadmap',
        mapTypeControl: false,
        streetViewControl: false,
        fullscreenControl: false
    };

    /**
     * Global marker object that holds all markers.
     * @type {Object.<string, google.maps.LatLng>}
     */
        //var markers = []; // array
    var markers = {}; // object

    /**
     * Concatenates given lat and lng with an underscore and returns it.
     * This id will be used as a key of marker to cache the marker in markers object.
     * @param {!number} lat Latitude.
     * @param {!number} lng Longitude.
     * @return {string} Concatenated marker id.
     */
    var getMarkerUniqueId = function (lat, lng) {
        return lat + '_' + lng;
    }

    /**
     * Creates an instance of google.maps.LatLng by given lat and lng values and returns it.
     * This function can be useful for getting new coordinates quickly.
     * @param {!number} lat Latitude.
     * @param {!number} lng Longitude.
     * @return {google.maps.LatLng} An instance of google.maps.LatLng object
     */
    var getLatLng = function (lat, lng) {
        return new google.maps.LatLng(lat, lng);
    };


    function initMap() {
        let directionsService = new google.maps.DirectionsService();
        let directionsRenderer = new google.maps.DirectionsRenderer();
        map = new google.maps.Map(document.getElementById('map'), myMapOptions);
        var boundaries = [
            {lat: -37.6490007, lng: 144.7435007},
            {lat: -37.6490007, lng: 145.3614997},

            {lat: -37.6490007, lng: 145.3614997},
            {lat: -38.2683988, lng: 145.3614997},

            {lat: -38.2683988, lng: 145.3614997},
            {lat: -38.2683988, lng: 144.7435007},

            {lat: -37.6490007, lng: 144.7435007},
            {lat: -38.2683988, lng: 144.7435007},
        ];
        var boundary = new google.maps.Polyline({
            path: boundaries,
            geodesic: true,
            strokeColor: '#ff5e44',
            strokeOpacity: 0.8,
            strokeWeight: 4,
        });
        boundary.setMap(map);

        // This event listener will call addMarker() when the map is clicked.
        map.addListener('click', function (event) {
            if (Object.keys(markers).length < 2) {
                var myLat = event.latLng.lat();
                var myLng = event.latLng.lng();
                if (myLat > bounds.south && myLat < bounds.north && myLng > bounds.west && myLng < bounds.east) {
                    addMarker(event.latLng);
                    if (document.getElementById("source").value == "")
                        document.getElementById("source").value = myLat + "_" + myLng;
                    else if (document.getElementById("source").value != "" && document.getElementById("target").value == "")
                        document.getElementById("target").value = myLat + "_" + myLng;
                    else if (document.getElementById("source").value == "" && document.getElementById("target").value != "")
                        document.getElementById("source").value = myLat + "_" + myLng;
                    document.getElementById("submit").disabled = false;              // 26/05/2020
                } else {
                    alert("Please choose a location that is inside the red rectangle.")
                }
            } else {
                alert("You have already selected the source and target locations. Press \"Clear\" if you want to delete the selected locations and choose new ones.");
            }


            //
            let i = 0;
            for (let m in markers)
                ++i;
            if (i === 2) {
                let coo = [];
                for (const [key, value] of Object.entries(markers)) {
                    coo.push(value.getPosition().lat());
                    coo.push(value.getPosition().lng());
                }
                //console.log(coo[0] + " " + coo[1] + " " + coo[2] + " " + coo[3]);

                directionsService.route({
                    origin: getLatLng(coo[0], coo[1]),
                    destination: getLatLng(coo[2], coo[3]),
                    provideRouteAlternatives: true,
                    travelMode: 'DRIVING',
                    drivingOptions: {
                        departureTime: new Date(tmr(new Date()))
                    }
                }, function (response, status) {
                    if (status == google.maps.DirectionsStatus.OK) {
                        document.getElementById("rx").value = "";
                        document.getElementById("ry").value = "";
                        document.getElementById("rz").value = "";
                        for (let i = 0, len = response.routes.length; i < len; i++) {
                            // get waypoints
                            let pointsArray = [];
                            pointsArray = response.routes[i].overview_path;
                            let midArr = Math.trunc(pointsArray.length / 2);

                            //console.log(i + " " + pointsArray.length + " " + midArr + " " + pointsArray[midArr].lat() + "-" + pointsArray[midArr].lng() + "\n")

                            if (i === 0)
                                document.getElementById("rx").value = pointsArray[midArr].lat() + "_" + pointsArray[midArr].lng();
                            else if (i === 1)
                                document.getElementById("ry").value = pointsArray[midArr].lat() + "_" + pointsArray[midArr].lng();
                            else
                                document.getElementById("rz").value = pointsArray[midArr].lat() + "_" + pointsArray[midArr].lng();

                        }
                    } else {
                        window.alert('Directions request failed due to ' + status);
                    }
                });
            }

        });
    }

    // Adds a marker to the map and push to the array or store it in markers object.
    function addMarker(location) {
        var lat = location.lat(); // lat of clicked point
        var lng = location.lng(); // lng of clicked point
        var markerId = getMarkerUniqueId(lat, lng); // cache this marker in markers object
        //console.log(lat + ", " + lng);

        var pinIcon = new google.maps.MarkerImage(
            "http://maps.google.com/mapfiles/kml/paddle/red-circle.png",
            null, /* size is determined at runtime */
            null, /* origin is 0,0 */
            null, /* anchor is bottom center of the scaled image */
            new google.maps.Size(80, 80)
        );

        //console.log(lat + ", " + lng);
        var marker = new google.maps.Marker({
            position: location,
            map: map,
            id: 'marker_' + markerId,
            icon: pinIcon,
        });

        //markers.push(marker);  // store this marker in the array
        markers[markerId] = marker;  // store this marker in the marker object
        bindMarkerEvents(marker); // bind right click event to marker
    }

    /**
     * Binds right click event to given marker and invokes a callback function that will remove the marker from map.
     * @param {!google.maps.Marker} marker A google.maps.Marker instance that the handler will binded.
     */
    var bindMarkerEvents = function (marker) {
        google.maps.event.addListener(marker, "rightclick", function (point) {
            var markerId = getMarkerUniqueId(point.latLng.lat(), point.latLng.lng()); // get marker id by using clicked point's coordinate
            var marker = markers[markerId]; // find marker
            removeMarker(marker, markerId); // remove it
        });
    };

    /**
     * Removes given marker from map.
     * @param {!google.maps.Marker} marker A google.maps.Marker instance that will be removed.
     * @param {!string} markerId Id of marker.
     */
    var removeMarker = function (marker, markerId) {
        marker.setMap(null); // set markers setMap to null to remove it from map
        var deleteId = marker.getPosition().lat() + "_" + marker.getPosition().lng();
        if (deleteId == document.getElementById("source").value)
            document.getElementById("source").value = "";
        else
            document.getElementById("target").value = "";
        delete markers[markerId]; // delete marker instance from markers object
    };

    /**
     * Removes the markers from the map.
     */
    function clearPage() {
        document.getElementById("source").value = "";
        document.getElementById("target").value = "";
        for (var m in markers) {
            markers[m].setMap(null);
            delete markers[m];
        }
        document.getElementById("popupHelp").style.display = "none";
    }

    function validateForm() {
        var i = 0;
        for (var m in markers) {
            ++i;
        }
        if (i < 2) {
            alert("Please pick a source and a target.");
            document.getElementById("submit").disabled = true;
        } else {
            document.getElementById("submit").disabled = false;
        }
    }

    let tmr = function tmr(today) {
        let departureTime = new Date(today)
        departureTime.setDate(departureTime.getDate() + 1)
        departureTime.setHours(2, 0, 0, 0)
        return departureTime
    }

    function openHelp() {
        document.getElementById("form-help").style.display="block";
        // var x = document.getElementById("popupHelp");
        // if (x.style.display === "none") {
        //     x.style.display = "block";
        // } else {
        //     x.style.display = "none";
        // }
    }
    function closeHelp() {
        document.getElementById("form-help").style.display="none";
    }
</script>
<script async defer
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDGoVkzP3U22-ylkWokeqDZHPNkxq0UvnA&callback=initMap">
</script>
</body>
</html>