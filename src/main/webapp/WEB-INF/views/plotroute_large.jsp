<%--
  Created by IntelliJ IDEA.
  User: llx
  Date: 18/02/2018
  Time: 9:48 AM
  To change this template use File | Settings | File Templates.
--%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <title>Alternative Routes Demo</title>
    <head>
        <style>
            /* Always set the map height explicitly to define the size of the div
             * element that contains the map. */
            #map, #simMap {
                height: 50%;
                width: 49.75%;
                float: left;
                border: 1px solid black;
            }

            #plMap, #penMap {
                height: 50%;
                width: 49.75%;
                float: right;
                border: 1px solid black;
            }

            /* Optional: Makes the sample page fill the window. */
            html, body {
                height: 100%;
                margin: 0;
                padding: 0;
            }

            #floating-panel {
                position: absolute;
                top: 2%;
                left: 0.6%;
                z-index: 5;
                /*width: 100px;*/
                width: 140px;
                height: 27px;
                background-color: #fff;
                padding: 5px;
                /*border: 1px solid #999;*/
                text-align: center;
                font-family: 'Roboto', 'sans-serif';
                line-height: 27px;
                opacity: 0.8;
                font-size: 25px;
            }

            #floating-plateau {
                position: absolute;
                top: 2%;
                left: 50.7%;
                z-index: 5;
                /*width: 100px;*/
                width: 140px;
                height: 27px;
                background-color: #fff;
                padding: 5px;
                /*border: 1px solid #999;*/
                text-align: center;
                font-family: 'Roboto', 'sans-serif';
                line-height: 27px;
                opacity: 0.8;
                font-size: 25px;
            }

            #floating-sim {
                position: absolute;
                top: 52%;
                left: 0.6%;
                z-index: 5;
                /*width: 100px;*/
                width: 140px;
                height: 27px;
                background-color: #fff;
                padding: 5px;
                /*border: 1px solid #999;*/
                text-align: center;
                font-family: 'Roboto', 'sans-serif';
                line-height: 27px;
                opacity: 0.8;
                font-size: 25px;
            }

            #floating-penalty {
                position: absolute;
                top: 52%;
                left: 50.7%;
                z-index: 5;
                /*width: 100px;*/
                width: 140px;
                height: 27px;
                background-color: #fff;
                padding: 5px;
                /*border: 1px solid #999;*/
                text-align: center;
                font-family: 'Roboto', 'sans-serif';
                line-height: 27px;
                opacity: 0.8;
                /*padding-left: 5px;*/
                font-size: 25px;
            }

            #mapLegend, #plMapLegend, #simMapLegend, #penMapLegend {
                background: #fdfdfd;
                color: #3c4750;
                padding: 10px;
                margin: 10px;
                font-weight: bold;
                /*filter: alpha(opacity=80);*/
                opacity: 0.8;
                border: 1px solid #000;
                display: inline-block;
                text-align: center;
                font-size: 20px;
            }

            #mapLegend div, #plMapLegend div, #simMapLegend div, #penMapLegend div {
                height: 28px;
                line-height: 25px;
                font-size: 1.35em;
            }

            #mapLegend span, #plMapLegend span, #simMapLegend span, #penMapLegend span {
                display: list-item;
                margin-left: 1.3em;
                list-style-type: square;
            }

            /*Survey*/
            .form-popup {
                display: none;
                text-align: left;
                position: absolute;
                width: 360px;
                /*left: 36.3%;*/
                /*top: 5%;*/
                left: 38.6%;
                top: 6.8%;
                transform: translate(-45%, 5%);
                z-index: 9;
                padding: 10px;
                opacity: 0.9;
                background-color: #AADAFF;
            }

            #floating-survey {
                position: absolute;
                top: 2%;
                left: 33%;
                z-index: 5;
                /*width: 160px;*/
                /*height: 51px;*/
                width: 300px;
                height: 61px;
                /*border: 1px solid #999;*/
            }

            .open-survey {
                background-color: #4A89F3;
                color: white;
                padding: 11px 19px;
                border: none;
                border-radius: 5px;
                cursor: pointer;
                position: absolute;
                /*font-size: large;*/
                font-size: xx-large;
                outline-color: darkkhaki;
            }

            /* Hover effects for buttons */
            .form-container .btn:hover, .open-survey:hover {
                opacity: 0.8;
            }
        </style>
    </head>
</head>

<body>

<div id="floating-panel">
    <b>Approach A</b>
</div>
<div id="floating-plateau">
    <b>Approach B</b>
</div>
<div id="floating-sim">
    <b>Approach C</b>
</div>
<div id="floating-penalty">
    <b>Approach D</b>
</div>

<div id="map"></div>
<div id="mapLegend"></div>

<div id="plMap"></div>
<div id="plMapLegend"></div>

<div id="simMap"></div>
<div id="simMapLegend"></div>

<div id="penMap"></div>
<div id="penMapLegend"></div>

<%--Survey--%>
<div id="floating-survey">
    <button class="open-survey" id="open-survey" onclick="openSurvey()"><strong>Submit Rating</strong></button>
</div>
<div class="form-popup" id="popupForm">
    <form:form commandName="parameter" id="submitForm" method="POST" action="submitSurvey">
        <p>Please rate each approach from 1 to 5 (higher is better). Please feel free to add comments if necessary</p>
        <hr>
        <td>
            <form:label path="mel">Are you currently living in Melbourne, or have you previously lived in Melbourne?</form:label>
<%--            <p>Are you currently living in Melbourne, or have you previously lived in Melbourne?</p>--%>
            <form:select path="mel" name="ml">
                <form:option id="ml" selected="false" value=""/>
                <form:options items="${mList}"/>
            </form:select>
        </td>
        <br><br>
        <td>
            <form:label path="goRate"><b style="padding-right: 5px">Approach A:</b></form:label>
            <form:select path="goRate" name="goRate">
                <form:option id="gm" selected="false" value=""/>
                <form:options items="${rtList}"/>
            </form:select>
        </td>
        <td>
            <form:label path="plRate"><b style="padding-right: 5px; padding-left: 30px">Approach B:</b></form:label>
            <form:select path="plRate" name="goRate">
                <form:option id="pl" selected="false" value=""/>
                <form:options items="${rtList}"/>
            </form:select>
        </td>
        <br><br>
        <td>
            <form:label path="simRate"><b style="padding-right: 5px">Approach C:</b></form:label>
            <form:select path="simRate" name="simRate">
                <form:option id="sim" selected="false" value=""/>
                <form:options items="${rtList}"/>
            </form:select>
        </td>
        <td>
            <form:label path="penRate"><b style="padding-right: 5px; padding-left: 30px">Approach D:</b></form:label>
            <form:select path="penRate" name="penRate">
                <form:option id="pen" selected="false" value=""/>
                <form:options items="${rtList}"/>
            </form:select>
        </td>
        <br><br>
        <form:label path="comment"><b>Comment(Optional):</b> </form:label>
        <br>
        <form:textarea style="margin: 0px; width: 320px; height: 81px;" id="comment" name="comment" path="comment"/>
        <br><br>
        <td><input id="submit" onclick="submitSurvey()" type="submit" value="Submit"/></td>
        <td><input onclick="closeSurvey()" type=button value="Close"></td>
    </form:form>
</div>
<%--Survey--%>

<script type="text/javascript">
    let map, plMap, simMap, penMap;
    let mps = [];

    //let mps = ['map', 'plMap', 'simMap', 'penMap'];

    let sourceLatLng = "${source}".split("_");
    let targetLatLng = "${target}".split("_");
    let colors = ['#4169e1', '#006400', '#ff00ff', '#ff0033', '#FFA500', '#4B0082'];

    let myMapOptions = {
        zoom: 10,
        center: {lat: -37.826844, lng: 145.060886},
        mapTypeId: 'roadmap',
        mapTypeControl: false,
        streetViewControl: false,
        fullscreenControl: false
    };

    function initMap() {
        let bounds = new google.maps.LatLngBounds();
        bounds.extend(getLatLng(sourceLatLng[0], sourceLatLng[1]));
        bounds.extend(getLatLng(targetLatLng[0], targetLatLng[1]));


        // Create map object
        map = new google.maps.Map(document.getElementById('map'), myMapOptions);
        mps.push(map);
        let legend = document.getElementById('mapLegend');

        // New legend
        let gLen = new Array();
        <c:forEach items = "${gLabels}" var = "ll">
        gLen.push(${ll})
        </c:forEach>

        let directionsService = new google.maps.DirectionsService;
        directionsService.route({
            origin: getLatLng(sourceLatLng[0], sourceLatLng[1]),
            destination: getLatLng(targetLatLng[0], targetLatLng[1]),
            provideRouteAlternatives: true,
            travelMode: 'DRIVING',
            drivingOptions: {
                departureTime: new Date(tmr(new Date()))
            }
        }, function (response, status) {
            if (status == google.maps.DirectionsStatus.OK) {
                let min;
                let sec;
                for (let i = 0, len = response.routes.length; i < len; i++) {
                    new google.maps.DirectionsRenderer({
                        map: map,
                        directions: response,
                        routeIndex: i,
                        polylineOptions: {strokeColor: colors[i], strokeWeight: 4.5, strokeOpacity: 0.8},
                        suppressMarkers: true
                    });

                    // get waypoints
                    //let pointsArray = [];
                    //pointsArray = response.routes[i].overview_path;
                    //let midArr = Math.trunc(pointsArray.length / 2);
                    //console.log(i + " " + pointsArray.length + " " + midArr + " " + pointsArray[midArr].lat() + "-" + pointsArray[midArr].lng() + "\n")

                    //let minutes = Math.trunc(response.routes[i].legs[0].duration.value / 60);
                    //let seconds = response.routes[i].legs[0].duration.value % 60;
                    //let travelTime = Math.round(response.routes[i].legs[0].duration.value / 60);
                    //var travelDist = (response.routes[i].legs[0].distance.value / 1000).toFixed(2);

                    if (gLen.length - 1 >= i) {
                        //min = Math.trunc(gLen[i]);
                        //sec = Math.round((gLen[i] - min) * 60);
                        min = Math.round(gLen[i]);
                    } else {
                        let size = gLen.length - 1;
                        let diff = Math.round(response.routes[i].legs[0].duration.value / 60) - Math.round(response.routes[size].legs[0].duration.value / 60);
                        if (diff > 0)
                            min = Math.round(gLen[size]) + diff;
                        else
                            min = Math.round(gLen[size]);

                        //min = min = Math.trunc(gLen[size]);
                        //sec = Math.round((gLen[size] - min) * 60);
                    }

                    // Add legends
                    let div = document.createElement('div');
                    //div.innerHTML = '<span style="color:' + colors[i] + '">Route ' + (i + 1) + ': ' + min + ' min ' + sec + ' sec</span>';
                    div.innerHTML = '<span style="color:' + colors[i] + '">Route ' + (i + 1) + ': ' + min + ' min</span>';
                    //div.innerHTML = '<span style="color:' + colors[i] + '">Route ' + (i + 1) + ': ' + min  + ' | ' + travelTime + ' min</span>';

                    // div.innerHTML = '<span style="color:' + colors[i] + '">Route ' + (i + 1) + ': ' + travelTime + ' min;' + travelDist + ' km</span>';
                    legend.appendChild(div);

                }
                //directionsDisplay.setDirections(response);
            } else {
                window.alert('Directions request failed due to ' + status);
            }
        });

        // Plateau
        plMap = new google.maps.Map(document.getElementById('plMap'), myMapOptions);
        mps.push(plMap);
        plMap.fitBounds(bounds);     // auto-zoom
        plMap.panToBounds(bounds);   // auto-center
        var plLegend = document.getElementById('plMapLegend');

        var pathArray = new Array();
        <c:forEach items = "${plRoutes}" var = "sp">
        var path = new Array();

        <c:forEach items = "${sp}" var = "add">
        var myLocation = new google.maps.LatLng(${add.lat}, ${add.lng});
        path.push(myLocation);
        </c:forEach>

        pathArray.push(path);
        </c:forEach>

        var lenArray = new Array();
        <c:forEach items = "${plLabels}" var = "ll">
        lenArray.push(${ll})
        </c:forEach>

        //let cur = 0;                      // Increasing plot
        let cur = pathArray.length - 1;     // Decreasing plot
        let renderArray = [];
        //if (pathArray.length > 0) {
        if (cur >= 0) {
            plotPolyLine(pathArray[cur], plMap, '1');
        }


        // Similarity
        simMap = new google.maps.Map(document.getElementById('simMap'), myMapOptions);
        mps.push(simMap);
        simMap.fitBounds(bounds);
        simMap.panToBounds(bounds);
        let simLegend = document.getElementById('simMapLegend');
        pathArray = new Array();
        <c:forEach items = "${simRoutes}" var = "sp">
        path = new Array();

        <c:forEach items = "${sp}" var = "add">
        myLocation = new google.maps.LatLng(${add.lat}, ${add.lng});
        path.push(myLocation);
        </c:forEach>

        pathArray.push(path);
        </c:forEach>

        lenArray = new Array();
        <c:forEach items = "${simLabels}" var = "ll">
        lenArray.push(${ll})
        </c:forEach>

        //let cur = 0;                      // Increasing plot
        cur = pathArray.length - 1;     // Decreasing plot
        renderArray = [];
        //if (pathArray.length > 0) {
        if (cur >= 0) {
            plotPolyLine(pathArray[cur], simMap, '2');
        }

        // Penalty
        penMap = new google.maps.Map(document.getElementById('penMap'), myMapOptions);
        mps.push(penMap);
        penMap.fitBounds(bounds);
        penMap.panToBounds(bounds);
        let penLegend = document.getElementById('penMapLegend');
        pathArray = new Array();
        <c:forEach items = "${penRoutes}" var = "sp">
        path = new Array();

        <c:forEach items = "${sp}" var = "add">
        myLocation = new google.maps.LatLng(${add.lat}, ${add.lng});
        path.push(myLocation);
        </c:forEach>

        pathArray.push(path);
        </c:forEach>

        lenArray = new Array();
        <c:forEach items = "${penLabels}" var = "ll">
        lenArray.push(${ll})
        </c:forEach>

        //let cur = 0;                      // Increasing plot
        cur = pathArray.length - 1;     // Decreasing plot
        renderArray = [];
        //if (pathArray.length > 0) {
        if (cur >= 0) {
            plotPolyLine(pathArray[cur], penMap, '3');
        }

        function plotPolyLine(singlePath, mapType, opt) {
            renderArray[cur] = new google.maps.Polyline({
                path: singlePath,
                geodesic: true,
                strokeColor: colors[cur],
                strokeOpacity: 0.8,
                strokeWeight: 4.5
            });
            renderArray[cur].setMap(mapType);

            // TODO: Similarity legend is not working

            //let minutes = Math.trunc(lenArray[cur]);
            //let seconds = Math.round((lenArray[cur] - minutes) * 60);


            let iDx = cur % (lenArray.length - 1);
            if (cur === 0)
                iDx = lenArray.length - 1;

            let div = document.createElement('div');
            div.innerHTML = '<span style="color:' + colors[iDx] + '">Route ' + (iDx + 1) + ': ' + Math.round(lenArray[iDx]) + ' min</span>';
            //div.innerHTML = '<span style="color:' + colors[cur] + '">Route ' + (cur + 1) + ': ' + minutes + ' min ' + seconds + ' sec</span>';
            if (opt === '1')
                plLegend.appendChild(div);
            else if (opt === '2')
                simLegend.appendChild(div);
            else
                penLegend.appendChild(div);

            //cur++;      increaseing plot
            cur--;
            //if (cur < pathArray.length) {
            if (cur >= 0) {
                plotPolyLine(pathArray[cur], mapType, opt);
            }
        }

        map.controls[google.maps.ControlPosition.LEFT_BOTTOM].push(legend);
        plMap.controls[google.maps.ControlPosition.LEFT_BOTTOM].push(plLegend);
        simMap.controls[google.maps.ControlPosition.LEFT_BOTTOM].push(simLegend);
        penMap.controls[google.maps.ControlPosition.LEFT_BOTTOM].push(penLegend);

        let sIcon = new google.maps.MarkerImage(
            "http://maps.google.com/mapfiles/kml/paddle/S.png",
            null, /* size is determined at runtime */
            null, /* origin is 0,0 */
            null, /* anchor is bottom center of the scaled image */
            new google.maps.Size(80, 80)
        );

        let tIcon = new google.maps.MarkerImage(
            "http://maps.google.com/mapfiles/kml/paddle/T.png",
            null, /* size is determined at runtime */
            null, /* origin is 0,0 */
            null, /* anchor is bottom center of the scaled image */
            new google.maps.Size(80, 80)
        );


        for (let i = 0; i < mps.length; ++i) {
            let sMarker = new google.maps.Marker({
                position: getLatLng(sourceLatLng[0], sourceLatLng[1]),
                map: mps[i],
                // label: {
                //     text: 's',
                //     color: 'white',
                //     fontSize: "15px"
                // },
                icon: sIcon,
                title: 'Source'
            });
            let tMarker = new google.maps.Marker({
                position: getLatLng(targetLatLng[0], targetLatLng[1]),
                map: mps[i],
                // label: {
                //     text: 't',
                //     color: 'white',
                //     fontSize: "15px"
                // },
                icon: tIcon,
                title: 'Target'
            });
        }
        // End of the init
    }

    let getLatLng = function (lat, lng) {
        return new google.maps.LatLng(lat, lng);
    };

    let tmr = function tmr(today) {
        let departureTime = new Date(today)
        departureTime.setDate(departureTime.getDate() + 1)
        departureTime.setHours(2, 0, 0, 0)
        return departureTime
    }

    // Survey
    function openSurvey() {
        document.getElementById("popupForm").style.display = "block";
    }

    // function submitSurvey() {
    //     document.getElementById("popupForm").style.display = "none";
    //     document.getElementById("open-survey").disabled = true;
    // }

    function submitSurvey() {
        let a = document.getElementById("goRate").value;
        let b = document.getElementById("plRate").value;
        let c = document.getElementById("simRate").value;
        let d = document.getElementById("penRate").value;
        let e = document.getElementById("mel").value;

        //if (a !== "" && b !== "" && c !== "" && d !== "") {
        if (a && b && c && d && e) {
            document.getElementById("popupForm").style.display = "none";
            document.getElementById("open-survey").disabled = true;
        }
    }

    function closeSurvey() {
        document.getElementById("popupForm").style.display = "none";
    }


    // Survey
</script>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script type="text/javascript">
    $('#submitForm').submit(function (e) {
        // reference to form object
        let frm = $('#submitForm');
        e.preventDefault();

        let fd = {}
        $.each(this, function (i, v) {
            let input = $(v);
            fd[input.attr("name")] = input.val();
            delete fd["undefined"];
            //console.log(input.attr("name") + " - " + input.val());
        });

        if (fd["goRate"] && fd["plRate"] && fd["simRate"] && fd["penRate"] && fd["mel"]) {

            $.ajax({
                type: $(frm).attr('method'),
                url: $(frm).attr('action'),
                //dataType: 'json',
                //contentType: 'application/json; charset=utf-8',
                //data: JSON.stringify(fd),
                data: fd,
                dataType: 'text',
                success: function (t) {
                    //alert("Response: Google: "+r.goRate+"  Plateau: "+r.plRate+"  Similarity: "+r.simRate+"  Penalty: "+r.penRate + " Comment" + r.comment);
                    alert("Thank you, we have received your rating. You may close this window now. If you want to give ratings " +
                        "for other source-target pairs, please go to the main page to select a different source and target pair.");
                    $(this).html("Success!");
                },
                error: function () {
                    alert("Submission Failed");
                    $(this).html("Error!");
                }
            });

        } else
            if (!fd["mel"])
                alert("Please answer Yes/No to whether you are living (or have lived) in Melbourne.");
            else
                alert("Please rate each approach before you submit.");
        return false;

    });
</script>

<script async defer
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDGoVkzP3U22-ylkWokeqDZHPNkxq0UvnA&callback=initMap">
</script>

</body>

</html>
